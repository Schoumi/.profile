<a href="https://www.davidrevoy.com/" title="Made by David Revoy"><img alt="A squirrel forges a golden acorn made by David Revoy for Forgejo" src="https://git.mob-dev.fr/Schoumi/.profile/raw/branch/master/images/Forgejo_by-David-Revoy.jpg"></a>

# Who am i?

---

Anthony Chomienne, Freelance Developer, Computer Engeneering Teacher at [CPE Lyon](https://cpe.fr). I develop for Android some free and open-source applications. 

# Current Projects

---

## PeerTube Live

Android application that let you livestream from your phone

More information on [https://codeberg.org/Schoumi/PeerTubeLive](https://codeberg.org/Schoumi/PeerTubeLive)

More information about issues and enhancement on [https://codeberg.org/Schoumi/PeerTubeLive/issues](https://codeberg.org/Schoumi/PeerTubeLive/issues)

## Goblim

Android application that upload photo on lutim instance and allow you to share the generated link to who you want.

More information on [https://git.mob-dev.fr/Schoumi/Goblim/](https://git.mob-dev.fr/Schoumi/Goblim/)

More information about issues and enhancement on [https://git.mob-dev.fr/Schoumi/Goblim/issues](https://git.mob-dev.fr/Schoumi/Goblim/issues)

## Blood donation

Android application that allow french people to know where they can give their blood to the French Blood Office (EFS). The application locate the user and give information about mobile and permanent collect.

More information on [https://git.mob-dev.fr/Schoumi/BloodDonation](https://git.mob-dev.fr/Schoumi/BloodDonation)

More information about issues and enhancement on [https://git.mob-dev.fr/Schoumi/BloodDonation/issues](https://git.mob-dev.fr/Schoumi/BloodDonation/issues)

## LPCProg

Android application that flash binaries on your NXP's LPC microcontrollers. It's a port of a Linux app to Android with some new functions

More information on [https://git.mob-dev.fr/Schoumi/lpcprog/](https://git.mob-dev.fr/Schoumi/lpcprog/)

More information about issues and enhancement on [https://git.mob-dev.fr/Schoumi/lpcprog/issues](https://git.mob-dev.fr/Schoumi/lpcprog/issues)

## Exodus (Not the maintener anymore, but the original developer of the Android App)

Android application that list permissions and trackers found by Exodus Privacy in Android applications installed on your smartphone from Google Play Store.

More information on sur [https://github.com/Exodus-Privacy/exodus-android-app](https://github.com/Exodus-Privacy/exodus-android-app)

More information about issues and enhancement on [https://github.com/Exodus-Privacy/exodus-android-app/issues](https://github.com/Exodus-Privacy/exodus-android-app/issues)

## Other projects

I have some other projects on my todo list. More information soon.

# Other Work

---

## Photography

I do some photography while i'm traveling. You can find some of them [https://pixels.schoumi.fr](https://pixels.schoumi.fr)

# Support My Work

---

Work on my free projects is something very cool and I would spend more time on them. This is why I accept donation to work more on my projects and less for clients.

You can do so using Liberapay:

<a href="https://liberapay.com/Schoumi/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

Or by using Ko-Fi:

<a href='https://ko-fi.com/P5P7LEKL2' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://storage.ko-fi.com/cdn/kofi5.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
